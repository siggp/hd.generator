# Run script
1. Download and install node.js

2. Add npm do your environmental variables

3. Install packages by npm (or yarn)
- go to cars.shop.api directory in command line
- execute "npm install"

4. Execute script via command "npm run car-shop-api"
- go to cars.shop.api directory in command line
- execute "npm run hd.generator"

5. In root directory output csv files should be created

# Import order for SQL:
1. Agent
2. Inwestycja
3. Klient
4. Budynek
5. Mieszkanie
6. lokal uslugowy
7. Kupno
8. wynajem
9. Wypłata za kupno
10. Wplata za wynajem

# Modify size of data
- modify src/services/config.porivder.ts file