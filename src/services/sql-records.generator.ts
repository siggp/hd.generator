import { FilesReader } from "./files.reader";
import { ISqlInput } from "../models/sql-input.interface";
import { Agent, Inwestycja, Klient, Budynek, Mieszkanie, Lokal_uslugowy, Kupno, Wynajem, Wplata_za_kupno, Wplata_za_wynajem, SqlModel, SqlDataCounts } from "../models/sql.model";
import { recordsGeneratorHelper } from "./records-generator.helper";


export class SqlRecordsGenerator {

    private inputJsonData: ISqlInput;
    private helper: recordsGeneratorHelper;

    constructor(
        jsonDataFilePath: string
    ) {
        this.inputJsonData = new FilesReader().readJson(jsonDataFilePath);
        this.helper = new recordsGeneratorHelper();
    }

    public generate(
        counts: SqlDataCounts,
        startYear: number,
        endYear: number,
        previousSqlModel?: SqlModel,
    ): SqlModel {
        let result: SqlModel = new SqlModel();
        result.Agent = this.generateAgents(counts.Agent);
        result.Inwestycja = this.generateInwestycja(counts.Inwestycja, startYear, endYear);
        result.Klient = this.generateKlient(counts.Klient);
        result.Budynek = this.generateBudynki(counts.Budynek, result.Inwestycja);
        result.Mieszkanie = this.generateMieszkania(counts.Mieszkanie, result.Budynek);
        result.Lokal_uslugowy = this.generateLocal(counts.Lokal_uslugowy, result.Budynek);
        result.Kupno = this.generateKupno(counts.Kupno, result.Agent, result.Mieszkanie, result.Klient, result.Lokal_uslugowy, startYear, endYear);
        result.Wynajem = this.generateWynajem(counts.Wynajem, result.Agent, result.Klient, result.Lokal_uslugowy, startYear, endYear);
        result.Wplata_za_kupno = this.generateWplataZaKupno(counts.WyplataZaKupno, result.Kupno, startYear, endYear);
        result.Wplata_za_wynajem = this.generateWplataZaWynajem(counts.WplataZaWynajem, result.Wynajem, startYear, endYear);

        if (previousSqlModel) {
            for (const property in result) {
                result[property] = previousSqlModel[property].concat(result[property]);
            }
        }

        return result
    }

    public makeChangesInLocation(
        model: SqlModel
    ) {
        model.Inwestycja
            .forEach(x => {
                if (x.lokalizacja === 'obrzeża miasta' && this.helper.getRandomNumber(1, 20) === 4) {
                    x.lokalizacja = 'centrum';
                }
            })
    }

    private generateAgents(
        numberOfRecords: number
    ): Agent[] {
        let agents: Agent[] = [];
        for (let i = 0; i < numberOfRecords; i++) {
            agents.push({
                Pesel: this.helper.getRandomPesel(),
                imie: this.helper.getRandomStringFromArray(this.inputJsonData.imie),
                nazwisko: this.helper.getRandomStringFromArray(this.inputJsonData.nazwisko),
                data_zatrudnienia: this.helper.getRandomDate(1995, 2015),
                pensja: this.helper.getRandomNumber(2300, 12000),
                nr_telefonu: this.helper.getRandomPhone(),
                data_urodzenia: this.helper.getRandomDate(1954, 1996),
            })
        }
        return agents;
    }

    private generateInwestycja(
        numberOfRecords: number,
        startDate: number,
        endDate: number
    ): Inwestycja[] {
        let inwestycje: Inwestycja[] = [];
        for (let i = 0; i < numberOfRecords; i++) {
             inwestycje.push({
                ID: i + 1,
                data_rozpoczecia: this.helper.getRandomDate(startDate, endDate),
                data_zakonczenia: this.helper.getRandomDate(startDate, endDate),
                kapital_na_realizacje: this.helper.getRandomNumber(5000000, 100000000),
                lokalizacja: this.helper.getRandomLocalization(),
                straty_poniesione_podczas_budowy: this.helper.getRandomNumber(100000, 5000000),
                uzyta_technologia: this.helper.getRandomNumber(1,2) === 1 ? 'prefabrykaty betonowe' : 'konstrukcje monolityczne',
                miasto: 'Gdańsk',
                dzielnica: this.helper.getRandomStringFromArray(this.inputJsonData.dzielnica)
            })
        }
        return inwestycje;
    }

    private generateKlient(
        numberOfRecords: number
    ): Klient[] {
        let klient: Klient[] = [];
        for (let i = 0; i < numberOfRecords; i++) {
            klient.push({
                Pesel: this.helper.getRandomPesel(),
                imie: this.helper.getRandomStringFromArray(this.inputJsonData.imie),
                nazwisko: this.helper.getRandomStringFromArray(this.inputJsonData.nazwisko),
                data_urodzenia: this.helper.getRandomDate(1949, 1997),
                nr_telefonu: this.helper.getRandomPhone()
            })
        }
        return klient;
    }

    private generateBudynki(
        numberOfRecords: number,
        inwestycje: Inwestycja[]
    ) {
        let budynek: Budynek[] = [];

        for (let i = 0; i < numberOfRecords; i++) {
            budynek.push({
                adres: this.helper.getRandomAdres(this.inputJsonData.ulica),
                calkowita_powierzchnia: this.helper.getRandomNumber(10000, 50000),
                FK_inwestycja: inwestycje[this.helper.getRandomIndexOfArray(inwestycje)].ID,
                koszt_budowy_budynku: this.helper.getRandomNumber(3000000, 2500000)
            })
        }
        return budynek;
    }

    private generateMieszkania(
        numberOfRecords: number,
        budynki: Budynek[]
    ) {
        let mieszkanie: Mieszkanie[] = [];

        for (let i = 0; i < numberOfRecords; i++) {
            mieszkanie.push({
                ID: i+1,
                ID_Budynek: budynki[this.helper.getRandomIndexOfArray(budynki)].adres,
                powierzchnia: this.helper.getRandomNumber(35, 150),
                pietro: this.helper.getRandomNumber(0, 17),
                ilosc_pokoi: this.helper.getRandomNumber(1, 7),
                czy_sprzedane: this.helper.getRandomNumber(1,15) !== 10,
                cena_kupna: this.helper.getRandomNumber(150000, 800000),
                nr: this.helper.getRandomNumber(1, 200)
            })
        }
        return mieszkanie;
    }

    private generateLocal(
        numberOfRecords: number,
        budynki: Budynek[]
    ) {
        let lokale: Lokal_uslugowy[] = [];

        for (let i = 0; i < numberOfRecords; i++) {
            lokale.push({
                ID: i+1,
                ID_Budynek: budynki[this.helper.getRandomIndexOfArray(budynki)].adres,
                powierzchnia: this.helper.getRandomNumber(35, 150),
                ilosc_pomieszczen: this.helper.getRandomNumber(1, 5),
                czy_wynajete: this.helper.getRandomNumber(1,15) !== 10,
                cena_wynajmu: this.helper.getRandomNumber(1000, 7000),
                czy_sprzedany: this.helper.getRandomNumber(1,15) !== 10,
                cena_kupna: this.helper.getRandomNumber(150000, 800000),
                nr: this.helper.getRandomNumber(1, 200)
            })
        }
        return lokale;
    }

    private generateKupno(
        numberOfRecords: number,
        agents: Agent[],
        mieszkania: Mieszkanie[],
        klient: Klient[],
        Lokal_uslugowy: Lokal_uslugowy[],
        startYear: number,
        endYear: number
    ) {
        let kupna: Kupno[] = [];

        for (let i = 0; i < numberOfRecords; i++) {
            kupna.push({
                ID: i+1,
                data_podpisania_umowy_deweloperskiej: this.helper.getRandomDate(startYear, endYear),
                wielkosc_zadatku: this.helper.getRandomNumber(10000, 150000),
                czy_wplacono_zadatek: this.helper.getRandomNumber(1,3) === 1,
                termin_wplaty_zadatku: this.helper.getRandomDate(startYear, endYear),
                data_podpisania_umowy_wlasciwej: this.helper.getRandomDate(startYear, endYear),
                czy_wplacono_cala_naleznosc: this.helper.getRandomNumber(1,3) === 1,
                FK_Agent: agents[this.helper.getRandomIndexOfArray(agents)].Pesel,
                FK_Mieszkanie: mieszkania[this.helper.getRandomIndexOfArray(mieszkania)].ID,
                FK_Klient: klient[this.helper.getRandomIndexOfArray(klient)].Pesel,
                FK_Lokal_uslugowy: Lokal_uslugowy[this.helper.getRandomIndexOfArray(Lokal_uslugowy)].ID
            })
        }
        return kupna;
    }

    private generateWynajem(
        numberOfRecords: number,
        agents: Agent[],
        klient: Klient[],
        Lokal_uslugowy: Lokal_uslugowy[],
        startYear: number,
        endYear: number
    ) {
        let wynajem: Wynajem[] = [];

        for (let i = 0; i < numberOfRecords; i++) {
            wynajem.push({
                ID: i+1,
                data_rozpoczecia: this.helper.getRandomDate(startYear, endYear),
                data_zakonczenia: this.helper.getRandomDate(startYear, endYear),
                FK_Agent: agents[this.helper.getRandomIndexOfArray(agents)].Pesel,
                FK_Klient: klient[this.helper.getRandomIndexOfArray(klient)].Pesel,
                FK_Lokal_uslugowy: Lokal_uslugowy[this.helper.getRandomIndexOfArray(Lokal_uslugowy)].ID
            })
        }
        return wynajem;
    }

    private generateWplataZaKupno(
        numberOfRecords: number,
        kupna: Kupno[],
        startYear: number,
        endYear: number
    ) {
        let wplataKupno: Wplata_za_kupno[] = [];

        for (let i = 0; i < numberOfRecords; i++) {
            wplataKupno.push({
                Data: this.helper.getRandomDate(startYear, endYear),
                FK_Kupno: kupna[this.helper.getRandomIndexOfArray(kupna)].ID,
                kwota: this.helper.getRandomNumber(15000, 150000),
                rodzaj: this.helper.getRandomNumber(1,3) === 1 ? 'zadatek' : 'wplata koncowa'
            })
        }
        return wplataKupno;
    }

    private generateWplataZaWynajem(
        numberOfRecords: number,
        wynajmy: Wynajem[],
        startYear: number,
        endYear: number
    ) {
        let wplataWynajem: Wplata_za_wynajem[] = [];

        for (let i = 0; i < numberOfRecords; i++) {
            wplataWynajem.push({
                Data: this.helper.getRandomDate(startYear, endYear),
                FK_Wynajem: wynajmy[this.helper.getRandomIndexOfArray(wynajmy)].ID,
                kwota: this.helper.getRandomNumber(15000, 150000),
            })
        }
        return wplataWynajem;
    }
}