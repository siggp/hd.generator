import { IDataset, IBuilding, IPerson, ILocal } from "../models/dataset.interface";
import { FilesReader } from "./files.reader";
import { IJsonInput } from "../models/json-input.interface";
import { recordsGeneratorHelper } from "./records-generator.helper";

export class JsonRecordsGenerator {

    private inputJsonData: IJsonInput;
    private helper: recordsGeneratorHelper;

    constructor(
        jsonDataFilePath: string
    ) {
        this.inputJsonData = new FilesReader().readJson(jsonDataFilePath);
        this.helper = new recordsGeneratorHelper();
    }

    public generate(
        recordsCount: number,
        startDate: Date,
        endDate: Date
    ): IDataset[] {
        let result: IDataset[] = [];

        for (let i = 0; i < recordsCount; i++) {
            result.push(this.generateSingleRecord(
                i,
                startDate.getFullYear(),
                endDate.getFullYear()
            ));
        }

        return result;
    }

    public makeChangesInLocation(
        model: IDataset[]
    ) {
        model.forEach(x => {
            if (x.budynek.lokalizacja === 'obrzeża miasta' && this.helper.getRandomNumber(1, 20) === 4) {
                x.budynek.lokalizacja = 'centrum';
            }
        })
    }

    private generateSingleRecord(
        id: number,
        startDate: number,
        endDate: number
    ): IDataset {
        return {
            osoba: this.generatePerson(),
            lokal: this.generateLocale(id),
            budynek: this.generateBuilding(),
            dataZakupu: this.helper.getRandomDate(startDate, endDate),
            naKredyt: !(this.helper.getRandomNumber(1, 4) === 3)
        }
    }
    
    private generatePerson(): IPerson {
        let age = this.helper.getRandomNumber(20, 68);

        let maritalStatus: 'singiel' | 'w zwiazku malzenskim'
            = this.helper.getRandomNumber(1, 6) === 2 ?
            'singiel' : 'w zwiazku malzenskim';

        let yearsAfterWedding
            = maritalStatus === 'singiel' ?
            null : this.helper.getRandomNumber(1, 25);

        if (maritalStatus === 'w zwiazku malzenskim' && (age - yearsAfterWedding) < 18) {
            yearsAfterWedding = age - 18;
        }

        return {
            wiek: age,
            zarobki: this.helper.getRandomNumber(2500, 15000),
            plec: (this.helper.getRandomNumber(1, 3) === 2) ? 'kobieta' : 'mezczyzna',
            zawod: this.helper.getRandomStringFromArray(this.inputJsonData.zawod),
            stanCywilny: maritalStatus,
            iloscLatPoSlubie: yearsAfterWedding
        }
    }

    private generateLocale(
        id: number
    ): ILocal {
        return {
            id: id,
            czyKupiony: !(this.helper.getRandomNumber(1, 35) === 4),
            rodzaj: this.helper.getRandomNumber(1, 10) === 5 ? 'lokal uslugowy' : 'mieszkanie',
            wynajmowany: !(this.helper.getRandomNumber(1, 4) === 1),
            cenaKupna: this.helper.getRandomNumber(180000, 1000000),
            powierzchnia: this.helper.getRandomNumber(25, 150),
            przeznaczenie: (this.helper.getRandomNumber(1, 5) === 3) ? 'inwestycja' : 'uzytek wlasny',
            iloscPokoi: this.helper.getRandomNumber(1, 7),
            pietro: this.helper.getRandomNumber(0, 17),
        };
    }

    private generateBuilding(): IBuilding {
        let cityIndex =
            Math.floor(Math.random() * (this.inputJsonData.miasta.length));

        let quarterIndex =
            Math.floor(Math.random() * (this.inputJsonData.miasta[cityIndex].dzielnice.length));
          
        return {
            miasto: this.inputJsonData.miasta[cityIndex].nazwa,
            dzielnica: this.inputJsonData.miasta[cityIndex].dzielnice[quarterIndex],
            lokalizacja: this.helper.getRandomLocalization(),
            iloscMieszkan: this.helper.getRandomNumber(10, 150),
            iloscLokaliUslugowych: this.helper.getRandomNumber(0, 25)
        }
    }
}