declare function require(name:string);

export class FilesReader{

    public readCsv(
        path: string
    ): any {
        const csvToJson = require('convert-csv-to-json');
        return csvToJson.getJsonFromCsv(path);
    }

    public readJson(
        path: string
    ): any {
        let reader = require('fs');
        return JSON.parse(reader.readFileSync(path, 'utf8'));
    }
}