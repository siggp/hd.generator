declare function require(name:string);

export class JsonBuilder {
    
    private outputName: string;
    private outputPath: string;
    private json: any;
    private createCsvWriter: any = require('csv-writer').createObjectCsvWriter

    public withOutputName(
        name: string
    ) {
        this.outputName = name;
        return this;
    }

    public withOutputPath(
        path: string
    ) {
        this.outputPath = path;
        return this;
    }

    public useJson(
        json: any
    ) {
        this.json = json;
        return this;
    }

    public build() {
        let writer = require('fs');
        writer.appendFile(`${this.outputPath}/${this.outputName}.json`, JSON.stringify(this.json));
    }
}