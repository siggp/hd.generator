declare function require(name:string);

export class CsvBuilder {
    
    private outputName: string;
    private outputPath: string;
    private json: any;
    private createCsvWriter: any = require('csv-writer').createObjectCsvWriter

    public withOutputName(
        name: string
    ) {
        this.outputName = name;
        return this;
    }

    public withOutputPath(
        path: string
    ) {
        this.outputPath = path;
        return this;
    }

    public useJson(
        json: any
    ) {
        this.json = json;
        return this;
    }

    public build() {
        this.json = this.json.filter(x => !x.toDelete);
        this.json.forEach(element => {
            delete element.toDelete
            if (element.start) {
                delete element.start;
            }
            if (element.end) {
                delete element.end;
            }           
        });

        let creator =
            this.createCsvWriter(this.getMetaData());

        creator.writeRecords(this.json)
            .then(() => {
                console.log(`${this.outputName} was created with ${this.json.length} records`);
            })
            .catch(() => {
                console.log("Error");
            })
    }

    private getMetaData() {
        return {
            path: `${this.outputPath}/${this.outputName}.csv`,
            header: this.getHeader()
        };
    }

    private getHeader(): any {
        let header = [];
        for (let property in this.json[0]) {
            header.push({
                id: property,
                title: property
            })
        }
        return header;
    }
}