import { SqlDataCounts } from "../models/sql.model";

export class ConfigProvider {
    public outputDirectoryPath = "./";
    public inputJsonDataFile = "./input/input-json-data.json";
    public inputSqlDataFile = "./input/input-sql-data.json";

    public firstStepRecordCount: number = 1000;
    public secondStepRecordCount: number = 10;
    public startDate: Date = new Date(1970,1,1);
    public midDate: Date = new Date(2016,1,1);
    public endDate: Date = new Date(2018,1,1);

    firstStepSqlDataCounts: SqlDataCounts = {
        Agent: 30,
        Inwestycja: 38,
        Klient: 8000,
        Budynek: 100,
        Mieszkanie: 6500,
        Lokal_uslugowy: 400,
        Kupno: 6200,
        Wynajem: 4000,
        WyplataZaKupno: 25000,
        WplataZaWynajem: 30000
    }

    secondStepSqlDataCounts: SqlDataCounts = {
        Agent: 10,
        Inwestycja: 10,
        Klient: 10,
        Budynek: 10,
        Mieszkanie: 10,
        Lokal_uslugowy: 10,
        Kupno: 10,
        Wynajem: 10,
        WyplataZaKupno: 10,
        WplataZaWynajem: 10
    } 
}