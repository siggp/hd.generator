export class recordsGeneratorHelper {
    public getRandomPesel(): string {
        let result = "";
        for (let i = 0; i < 11; i++) {
            result += Math.floor(Math.random() * 9) + 1;
        }
        return result.toString();
    }

    public getRandomPhone(): string {
        let result = "";
        for (let i = 0; i < 9; i++) {
            result += Math.floor(Math.random() * 9) + 1;
        }
        return result.toString();
    }

    public getRandomStringFromArray(list: string[]): string {
        let index = Math.floor(Math.random() * list.length);
        return list[index];  
    }

    public getRandomDate(
        startYear: number,
        endYear: number
    ): string {
        let start = new Date(startYear,1,1);
        let end = new Date(endYear,1,1)
        let startInMs: number = start.getTime();
        let endInMs: number = end.getTime();
        let date = new Date(startInMs + Math.random() * (endInMs - startInMs));
        if (date.getMonth() === 0) {
            date.setMonth(1);
        }

        if (date.getMonth() === 1 && date.getDate() > 27) {
            date.setMonth(1);
        }

        if(!(date.getDate() < 20)) {
            date.setDate(date.getDate() - 1);
        }

        let result = '';
        result = result + date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();
        return result;
    } 
    
    public getRandomNumber(
        min: number,
        max: number
    ): number {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

    public getRandomLocalization(): 'centrum' | 'obrzeża miasta' | 'poza miastem' {
        let random: number = Math.floor(Math.random() * 3);
        if (random === 0) {
            return 'centrum';
        } else if (random === 1) {
            return 'obrzeża miasta';
        } else {
            return 'poza miastem';
        }
    }

    public getRandomAdres(
        streets: string[]
    ): string {
        let index = Math.floor(Math.random() * streets.length);
        let number = this.getRandomNumber(1, 200);
        return (streets[index] + " " + number);
    }

    public getRandomIndexOfArray(
        randArray: any[]
    ) {
        return this.getRandomNumber(0, randArray.length - 1);
    }
}