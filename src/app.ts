import { JsonRecordsGenerator } from "./services/json-records.generator";
import { JsonBuilder } from "./services/json.builder";
import { ConfigProvider } from "./services/config.provider";
import { IDataset } from "./models/dataset.interface";
import { SqlModel } from "./models/sql.model";
import { SqlRecordsGenerator } from "./services/sql-records.generator";
import { CsvBuilder } from "./services/csv.builder";

export class Application {

    private jsonRecordsGenerator: JsonRecordsGenerator;
    private sqlRecordsGenerator: SqlRecordsGenerator;
    private config: ConfigProvider;

    constructor() {
        this.config = new ConfigProvider();
        this.jsonRecordsGenerator = new JsonRecordsGenerator(this.config.inputJsonDataFile);
        this.sqlRecordsGenerator = new SqlRecordsGenerator(this.config.inputSqlDataFile);
    }

    public run() {
        this.manageJsonDataProvider();
        this.manageSqlDataProvider();
    }

    private manageSqlDataProvider() {
        let firstStepSqlModel = 
            this.sqlRecordsGenerator.generate(
                this.config.firstStepSqlDataCounts,
                this.config.startDate.getFullYear(),
                this.config.endDate.getFullYear()
            );

        let secondStepSqlModel = 
            this.sqlRecordsGenerator.generate(
                this.config.secondStepSqlDataCounts,
                this.config.startDate.getFullYear(),
                this.config.endDate.getFullYear(),
                firstStepSqlModel
            )

        for (let property in firstStepSqlModel) {
            new CsvBuilder()
                .withOutputName(property)
                .withOutputPath(this.config.outputDirectoryPath)
                .useJson(firstStepSqlModel[property])
                .build();
        }

        this.sqlRecordsGenerator.makeChangesInLocation(secondStepSqlModel);

        for (let property in secondStepSqlModel) {
            new CsvBuilder()
                .withOutputName(property + '2')
                .withOutputPath(this.config.outputDirectoryPath)
                .useJson(secondStepSqlModel[property])
                .build();
        }
    }

    private manageJsonDataProvider() {
        let firstStepRecords: IDataset[]  = 
            this.jsonRecordsGenerator.generate(this.config.firstStepRecordCount, this.config.startDate, this.config.midDate);

        let secondStepRecords: IDataset[]  = 
            this.jsonRecordsGenerator.generate(this.config.secondStepRecordCount, this.config.midDate, this.config.endDate);

        new JsonBuilder()
            .withOutputName("recordsStep1JSON")
            .withOutputPath(this.config.outputDirectoryPath)
            .useJson(firstStepRecords)
            .build();

        this.jsonRecordsGenerator.makeChangesInLocation(secondStepRecords);

        new JsonBuilder()
            .withOutputName("recordsStep2JSON")
            .withOutputPath(this.config.outputDirectoryPath)
            .useJson(firstStepRecords.concat(secondStepRecords))
            .build();
    }
}