import { Application } from "./app";

const main: () => void = () => {
    new Application().run();
}

main();