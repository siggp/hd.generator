export class SqlModel {
    "Inwestycja": Inwestycja[];
    "Budynek": Budynek[];
    "Mieszkanie": Mieszkanie[];
    "Lokal_uslugowy": Lokal_uslugowy[];
    "Agent": Agent[];
    "Klient": Klient[];
    "Kupno": Kupno[];
    "Wynajem": Wynajem[];
    "Wplata_za_kupno": Wplata_za_kupno[];
    "Wplata_za_wynajem": Wplata_za_wynajem[];
}

export interface Inwestycja {
    ID: number;
    data_rozpoczecia: string;
    data_zakonczenia: string;
    kapital_na_realizacje: number;
    lokalizacja: 'centrum' | 'obrzeża miasta' | 'poza miastem';
    straty_poniesione_podczas_budowy: number;
    uzyta_technologia: 'prefabrykaty betonowe' | 'konstrukcje monolityczne';
    miasto: string;
    dzielnica: string;
}

export interface Budynek {
    adres: string;
    calkowita_powierzchnia: number;
    FK_inwestycja: number;
    koszt_budowy_budynku: number;
}

export interface Mieszkanie {
    ID: number;
    ID_Budynek: string;
    powierzchnia: number;
    pietro: number;
    ilosc_pokoi: number;
    czy_sprzedane: boolean;
    cena_kupna: number;
    nr: number;
}


export interface Lokal_uslugowy {
    ID: number;
    ID_Budynek: string;
    powierzchnia: number;
    ilosc_pomieszczen: number;
    czy_wynajete: boolean;
    cena_wynajmu: number;
    czy_sprzedany: boolean;
    cena_kupna: number;
    nr: number;
}

export interface Agent {
    Pesel: string;
    imie: string;
    nazwisko: string;
    data_zatrudnienia: string;
    pensja: number
    nr_telefonu: string;
    data_urodzenia: string;
}

export interface Klient {
    Pesel: string;
    imie: string;
    nazwisko: string;
    data_urodzenia: string;
    nr_telefonu: string;
}

export interface Kupno {
    ID: number;
    data_podpisania_umowy_deweloperskiej: string;
    wielkosc_zadatku: number;
    czy_wplacono_zadatek: boolean;
    termin_wplaty_zadatku: string;
    data_podpisania_umowy_wlasciwej: string;
    czy_wplacono_cala_naleznosc: boolean;
    FK_Agent: string;
    FK_Mieszkanie: number;
    FK_Klient: string;
    FK_Lokal_uslugowy: number;
}
export interface Wynajem {
    ID: number;
    data_rozpoczecia: string;
    data_zakonczenia: string;
    FK_Agent: string;
    FK_Klient: string;
    FK_Lokal_uslugowy: number;
}
export interface Wplata_za_kupno {
    Data: string;
    FK_Kupno: number;
    kwota: number;
    rodzaj: 'zadatek' | 'transza' | 'wplata koncowa';
}

export interface Wplata_za_wynajem {
    Data: string;
    FK_Wynajem: number;
    kwota: number;
}

export class SqlDataCounts {
    Agent: number;
    Inwestycja: number;
    Klient: number;
    Budynek: number;
    Mieszkanie: number;
    Lokal_uslugowy: number;
    Kupno: number;
    Wynajem: number;
    WyplataZaKupno: number;
    WplataZaWynajem: number;
}