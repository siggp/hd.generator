export interface IJsonInput {
    "zawod": string[],
    "miasta": ICity[]
}

export interface ICity {
    "nazwa": string,
    "dzielnice": string[]
}