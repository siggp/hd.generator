import { ICity } from "./json-input.interface";

export interface ISqlInput {
    "imie": string[];
    "nazwisko": string[];
    "ulica": string[];
    "dzielnica": string[];
}