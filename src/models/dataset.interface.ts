export interface IDataset {
    osoba: IPerson;
    lokal: ILocal;
    budynek: IBuilding;
    dataZakupu: string;
    naKredyt: boolean;
}

export interface IPerson {
    wiek: number;
    zarobki: number;
    plec: 'kobieta' | 'mezczyzna';
    zawod: string;
    stanCywilny: 'singiel' | 'w zwiazku malzenskim';
    iloscLatPoSlubie?: number;
}

export interface ILocal {
    id: number,
    czyKupiony: boolean,
    rodzaj: 'mieszkanie' | 'lokal uslugowy',
    wynajmowany: boolean,
    cenaKupna: number,
    powierzchnia: number,
    przeznaczenie: 'inwestycja' | 'uzytek wlasny',
    iloscPokoi: number,
    pietro: number
}

export interface IBuilding {
    miasto: string,
    dzielnica: string,
    lokalizacja: 'centrum' | 'obrzeża miasta' | 'poza miastem';
    iloscMieszkan: number,
    iloscLokaliUslugowych: number
}